package com.hsuk.video.views.consumer.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsuk.video.consumer.dao.service.VideoViewsCountDao;
import com.hsuk.video.consumer.dto.VideoViewsCountDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class VideoViewsCountEventListener {

    @Autowired
    private VideoViewsCountDao videoViewsCountDao;

    private final ObjectMapper objectMapper = new ObjectMapper();


    public void receiveMessage(byte[] message) throws IOException {

        log.info("Received VideoCount Message: {}", new String(message));

        VideoViewsCountDto countDto = objectMapper.readValue(message, VideoViewsCountDto.class);
        try {
            videoViewsCountDao.saveViewsCount(Arrays.asList(countDto));
        } catch (Exception ex) {
            log.error("Error saving videocount data to DB. Sending to deadletter queue. {}", ex.getMessage());
            sendToDeadLetterQueue(Arrays.asList(countDto));
        }

    }

    public void sendToDeadLetterQueue(List<VideoViewsCountDto> countDtos) {
        //TODO: Send to dead letter queue in rabbitmq
    }
}
