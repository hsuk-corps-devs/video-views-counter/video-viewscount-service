package com.hsuk.video.views.consumer.api.config;

import com.hsuk.platform.utils.dynaconfig.util.DynamicPropUtil;
import com.hsuk.video.views.consumer.api.service.VideoViewsCountEventListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Slf4j
@Configuration
@EnableCaching
@PropertySources(value = {
        @PropertySource(name = "video.view.counter.db.global", value = "classpath:/db.properties"),
        @PropertySource(name = "video.view.counter.application", value = "classpath:/application.properties"),
        @PropertySource(name = "video.view.counter.application.env", value = "classpath:/application-${app.environment}.properties"),
        @PropertySource(name = "video.view.counter.db.env", value = "classpath:/db-${app.environment}.properties"),
        @PropertySource(name = "video.view.counter.override", value = "${loader.override.filepath}", ignoreResourceNotFound = true)
}
)
@ComponentScan({"com.hsuk.video.consumer", "com.hsuk.platform.utils"})
public class AppConfig {

    @Autowired
    DynamicPropUtil dynamicPropUtil;

    @Autowired
    private ConnectionFactory rabbitConnectionFactory;

    @Bean
    Queue queue(@Autowired DynamicPropUtil dynamicPropUtil) {
        final String queueName = dynamicPropUtil.getStr("rabbitmq.queue.name", "viewscount");
        return new Queue(queueName, true);
    }

    @Bean
    TopicExchange exchange(@Autowired DynamicPropUtil dynamicPropUtil) {
        return new TopicExchange(dynamicPropUtil.getStr("rabbitmq.topic.exchange", "viewscount-exchange"));
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        final String routingKey = dynamicPropUtil.getStr("rabbitmq.routing.key", "viewscount.#");
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        final String queueName = dynamicPropUtil.getStr("rabbitmq.queue.name", "viewscount");
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(VideoViewsCountEventListener receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

}