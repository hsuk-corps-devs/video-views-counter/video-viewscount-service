package com.hsuk.video.views.consumer.api.exception;

public class VideoNotFoundException extends RuntimeException {

    public VideoNotFoundException(String msg) {
        super(msg);
    }
}
