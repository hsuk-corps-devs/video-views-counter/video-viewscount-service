package com.hsuk.video.views.consumer.api;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ViewsConsumerAppMain {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ViewsConsumerAppMain.class).run(args);
    }
}
